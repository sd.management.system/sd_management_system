class Notification::ToDo < Notification
  default_scope { where(notifiable_type: 'ToDo') }
end
