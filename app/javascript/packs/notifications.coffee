document.addEventListener 'turbolinks:load', () ->
  $('.notifications .notification_resolve_update').on 'click', () ->
    notification_resolve_update(this)

notification_resolve_update = (element) ->
  $.ajax({
    method: 'PATCH',
    url: '/notifications/update_resolved',
    data: { id: $(element).prop('name'), resolve: $(element).prop('checked') },
  }).done((response) ->
    unless response.success
      alert(response.message)
      $(element).prop('checked', !$(element).prop("checked"))
  )
  return 0
