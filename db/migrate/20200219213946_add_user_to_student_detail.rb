class AddUserToStudentDetail < ActiveRecord::Migration[6.0]
  def change
    add_reference :student_details, :user
  end
end
