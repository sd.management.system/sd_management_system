class AddCountryToStudentDetail < ActiveRecord::Migration[6.0]
  def change
    add_column :student_details, :country, :string
    add_column :student_details, :country_of_birth, :string
    remove_column :student_details, :place_of_birth
  end
end
