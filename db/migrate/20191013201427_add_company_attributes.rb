class AddCompanyAttributes < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :certificate_exp_date, :date
    add_column :companies, :functioning_exp_date, :date
    add_column :companies, :access_key_exp_date, :date
  end
end
