class RemoveNotNullForVehiclesCompany < ActiveRecord::Migration[6.0]
  def change
    change_column :vehicles, :company_id, :integer, null: true
  end
end
