module Notifier::Type
  class ToDo < Notifier::Type::Base
    ATTRIBUTES = %i[end_date priority].freeze
    alias to_do resource

    def message_for(notification)
      notification.notifiable.title + ' : ' + (notification.notifiable.end_date || "prioritate #{notification.notifiable.priority}")
    end
  end
end
