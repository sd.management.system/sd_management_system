class CreateDrivingCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :driving_categories do |t|
      t.string :name
      t.decimal :price, precision: 8, scale: 2, default: 0
      t.references :company

      t.timestamps
    end
  end
end
