class DashboardController < ApplicationController
  include AuthorizableResource
  def show
    authorize :dashboard, :show?
  end
end
