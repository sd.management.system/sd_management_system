class CreateExtraServicesStudentDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :extra_services_student_details do |t|
      t.references :extra_service, null: false, foreign_key: true
      t.references :student_detail, null: false, foreign_key: true

      t.timestamps
    end
  end
end
