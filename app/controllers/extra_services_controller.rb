class ExtraServicesController < ApplicationController
  include AuthorizableResource
  before_action :authorize_resources, only: %i[
    index autocomplete
  ]
  before_action :authorize_resource, only: %i[
    show edit update destroy
  ]

  def index; end

  def new
    @extra_service = ExtraService.new
    authorize(@extra_service)
  end

  def create
    @extra_service = current_company
                          .extra_services
                          .new(extra_service_params)
    authorize(@extra_service)
    if @extra_service.save
      redirect_to extra_service_path(@extra_service), notice: 'Categorie adaugat cu success'
    else
      render :new
    end
  end

  def show; end

  def edit
    @extra_service = ExtraService.find(params[:id])
    authorize(@extra_service)
  end

  def update
    if @extra_service.update(extra_service_params)
      flash[:success] = 'Categories schimbat cu success'
      redirect_to extra_service_path(@extra_service)
    else
      render :edit
    end
  end

  def destroy
    @extra_service.destroy
    flash[:success] = 'Categories stears cu success'
    redirect_to extra_services_path
  end

  def autocomplete
    render json: autocomplete_response_for(@extra_services)
  end

  private

  def authorize_resources
    @extra_services = policy_scope(ExtraService)
  end

  def authorize_resource
    @extra_service = ExtraService.find(params[:id])
    authorize @extra_service
  end

  def extra_service_params
    params.require(:extra_service)
      .permit(policy(:extra_service).permitted_attributes)
  end
end
