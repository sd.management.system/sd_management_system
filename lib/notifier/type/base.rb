module Notifier::Type
  # represent basic notification functionality
  # basic functionality extends with different classes
  class Base
    attr_accessor :resource

    def initialize(resource)
      @resource = resource
    end

    # create notifications for resource
    def create_notifications
      self.class::ATTRIBUTES.each do |notifiable_attribute|
        next unless resource.send(notifiable_attribute).present?

        notification = notification_class.find_or_initialize_by(notifiable_id: resource.id, notifiable_attribute: notifiable_attribute)
        notification.order_date = order_date(notification)
        notification.save
      end
    end

    # retreive all notifications for resource
    def all
      notification_class.where(notifiable_id: resource.id)
    end

    # retreive notifications for multiple resources
    def self.batch_notifications(batch_objects)
      notification_class.where(notifiable_id: batch_objects.pluck(:id))
    end

    def message_for(notification)
      message = I18n.t "activerecord.models.#{notifiable_class_name.underscore}", count: 1
      message += ' - '
      message += I18n.t("activerecord.attributes.#{notifiable_class_name.underscore}.#{notification.notifiable_attribute}")
      message += ": #{resource.send(notification.notifiable_attribute)}"
    end

    # define dynamic names
    #
    def self.notifiable_class_name
      name.split('::').last
    end
    delegate :notifiable_class_name, to: :class

    def self.notifiable_class
      notifiable_class_name.constantize
    end
    delegate :notifiable_class, to: :class

    def self.notification_class_name
      "Notification::#{name.split('::').last}"
    end
    delegate :notification_class_name, to: :class

    def self.notification_class
      notification_class_name.constantize
    end
    delegate :notification_class, to: :class
    #
    # define dynamic names

    private

    def order_date(notification)
      date = resource.send(notification.notifiable_attribute)
      return date if date.is_a?(Date)

      case date
      when :scazuta
        5.days.from_now
      when :medie
        3.days.from_now
      when :ridicata
        Date.tomorrow
      else
        7.days.from_now
      end
    end
  end
end
