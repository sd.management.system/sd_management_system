class Vehicle < ApplicationRecord
  include Notification::NotifiableBase

  # TODO: use categories relation
  CATEGORIES = ['AM', 'A1', 'A2', 'A', 'B', 'BE', 'B extins', 'C', 'CE', 'D', 'DE'].freeze

  serialize :category, Array

  before_save :eliminate_blank_category

  belongs_to :company
  belongs_to :instructor_detail
  has_one :instructor,
          through: :instructor_detail,
          source: :user

  private

  def eliminate_blank_category
    category.delete('')
  end
end
