ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
  if instance.is_a? ActionView::Helpers::Tags::Label
    html_tag
  else
    class_attr_index = html_tag.index 'class="'
    if class_attr_index
      html_tag.insert(class_attr_index + 7, 'is-invalid ')
    else
      html_tag.insert(html_tag.index('>'), ' class="is-invalid"')
    end

    "<div class='col-sm-12'><small class='text-danger'>#{[*instance.error_message].join(', ')}</small></div>".html_safe + html_tag
  end
end
