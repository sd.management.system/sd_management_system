# frozen_string_literal: true
module Detailable
  module User
    def id_doc_series
      return nil unless role.in?(%w[instructor student])

      student_detail&.id_doc_series ||
        instructor_detail&.id_doc_series ||
        (raise 'Method undefined on detailable details')
    end

    def id_doc_number
      return nil unless role.in?(%w[instructor student])

      student_detail&.id_doc_number ||
        instructor_detail&.id_doc_number ||
        (raise 'Method undefined on detailable details')
    end

    def id_doc_cnp
      return nil unless role.in?(%w[instructor student])

      student_detail&.id_doc_cnp ||
        instructor_detail&.id_doc_cnp ||
        (raise 'Method undefined on detailable details')
    end

    def id_doc_exp_date
      return nil unless role.in?(%w[instructor student])

      student_detail&.id_doc_exp_date ||
        instructor_detail&.id_doc_exp_date ||
        (raise 'Method undefined on detailable details')
    end

    def id_doc_type
      return nil unless role.in?(%w[instructor student])

      student_detail&.id_doc_type ||
        instructor_detail&.id_doc_type ||
        (raise 'Method undefined on detailable details')
    end

    # > instructor detail attributes
    def license_series
      instructor_detail&.license_series
    end

    def license_exp_date
      instructor_detail&.license_exp_date
    end

    def certificate_series
      instructor_detail&.certificate_series
    end

    def certificate_number
      instructor_detail&.certificate_number
    end

    def certificate_exp_date
      instructor_detail&.certificate_exp_date
    end

    def medical_number
      instructor_detail&.medical_number
    end

    def medical_exp_date
      instructor_detail&.medical_exp_date
    end

    def record_number
      instructor_detail&.record_number
    end

    def record_exp_date
      instructor_detail&.record_exp_date
    end

    def psychological_number
      instructor_detail&.psychological_number
    end

    def psychological_exp_date
      instructor_detail&.psychological_exp_date
    end
    # < instructor detail attributes

    # > student detail attributes
    def old_name
      student_detail.old_name
    end

    def father_first_name
      student_detail.father_first_name
    end

    def mother_first_name
      student_detail.mother_first_name
    end

    def birthdate
      student_detail.birthdate
    end

    def sex
      student_detail.sex
    end

    def phone_number
      student_detail.phone_number
    end

    def contact_email
      student_detail.contact_email
    end

    def citizenship
      student_detail.citizenship
    end

    def county
      student_detail.county
    end

    def city
      student_detail.city
    end

    def street
      student_detail.street
    end

    def number
      student_detail.number
    end

    def building_number
      student_detail.building_number
    end

    def entrance_number
      student_detail.entrance_number
    end

    def floor
      student_detail.floor
    end

    def apartment
      student_detail.apartment
    end

    def place_of_birth
      student_detail.place_of_birth
    end

    def county_of_birth
      student_detail.county_of_birth
    end

    def city_of_birth
      student_detail.city_of_birth
    end

    def contract_number
      student_detail.contract_number
    end

    def contract_date
      student_detail.contract_date
    end

    def group_date
      student_detail.group_date
    end

    def duration_in_weeks
      student_detail.duration_in_weeks
    end

    def national_register_number
      student_detail.national_register_number
    end

    def contract_series
      student_detail.contract_series
    end

    def contract_group
      student_detail.contract_group
    end

    def discount_type
      student_detail.discount_type
    end

    def discount_value
      student_detail.discount_value
    end

    def discount_reason
      student_detail.discount_reason
    end
    # < student detail attributes
  end
end
