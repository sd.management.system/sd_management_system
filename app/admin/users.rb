ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation, :role, :company_id

  index do
    selectable_column
    id_column
    column :email
    column :company_id
    column :current_sign_in_at
    column :created_at
    actions
  end

  filter :email
  filter :company_id
  filter :current_sign_in_at
  filter :created_at

  show do
    attributes_table do
      row :email
      row :role
      row :company_id
      row :current_sign_in_at
      row :created_at
    end
  end

  controller do
    def update_resource(object, attributes)
      if attributes.first[:password].present?
        object.update(*attributes)
      else
        object.update_without_password(*attributes)
      end
    end
  end
end
