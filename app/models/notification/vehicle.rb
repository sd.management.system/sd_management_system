class Notification::Vehicle < Notification
  default_scope { where(notifiable_type: 'Vehicle') }
end
