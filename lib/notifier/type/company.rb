module Notifier::Type
  class Company < Notifier::Type::Base
    ATTRIBUTES = %i[certificate_exp_date functioning_exp_date access_key_exp_date].freeze
    alias company resource
  end
end
