class RenameAddressFieldsForStudentDetails < ActiveRecord::Migration[6.0]
  def change
    rename_column :student_details, :telephone, :phone_number

    rename_column :student_details, :country, :address_country
    rename_column :student_details, :county, :address_county
    rename_column :student_details, :city, :address_city
    rename_column :student_details, :street, :address_street
    rename_column :student_details, :number, :address_number
    rename_column :student_details, :building_number, :address_building_number
    rename_column :student_details, :entrance_number, :address_entrance_number
    rename_column :student_details, :floor, :address_floor
    rename_column :student_details, :apartment, :address_apartment
  end
end
