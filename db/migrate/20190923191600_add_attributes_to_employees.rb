class AddAttributesToEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :id_series, :string
    add_column :employees, :id_number, :string
    add_column :employees, :id_uniq_id, :string
    add_column :employees, :id_exp_date, :date
    add_column :employees, :license_series, :string
    add_column :employees, :license_exp_date, :date
    add_column :employees, :certificate_series, :string
    add_column :employees, :certificate_number, :string
    add_column :employees, :certificate_exp_date, :date
    add_column :employees, :medical_number, :string
    add_column :employees, :medical_exp_date, :date
    add_column :employees, :record_number, :string
    add_column :employees, :record_exp_date, :date
  end
end
