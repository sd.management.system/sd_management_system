ActiveAdmin.register Company do
  include Pundit
  permit_params CompanyPolicy.new(:admin_user, :company).permitted_attributes

  filter :title

  index do
    selectable_column
    id_column
    column :title
    column :uniq_title
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
    end
  end

  show do
    attributes_table do
      row :title
    end
  end

  # set admin action to true so validations are not necessary
  #
  before_create do
    resource.admin_action = true
  end
  before_update do
    resource.admin_action = true
  end
  #
  # #

  action_item :generate_new_admin_user, only: :show do
    link_to 'Generare administrator companie', generate_new_admin_user_admin_company_path(resource), method: :post
  end
  member_action :generate_new_admin_user, method: :post do
    admin = resource.users
                    .administrator
                    .new(email: resource.uniq_title, password: '12345678', first_name: 'admin', last_name: 'admin')
    admin.save
    redirect_to resource_path, notice: "adaugat"
  end
end
