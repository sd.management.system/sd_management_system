# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_19_163303) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "comments", force: :cascade do |t|
    t.string "commentable_type", null: false
    t.bigint "commentable_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "title"
    t.string "color"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "certificate_exp_date"
    t.date "functioning_exp_date"
    t.date "access_key_exp_date"
    t.string "uniq_title"
    t.string "address_county"
    t.string "address_city"
    t.string "address_street"
    t.string "address_number"
    t.string "address_building_number"
    t.string "address_entrance_number"
    t.string "address_floor"
    t.string "address_apartment"
  end

  create_table "driving_categories", force: :cascade do |t|
    t.string "name"
    t.decimal "price", precision: 8, scale: 2, default: "0.0"
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_driving_categories_on_company_id"
  end

  create_table "driving_categories_student_details", force: :cascade do |t|
    t.bigint "driving_category_id", null: false
    t.bigint "student_detail_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["driving_category_id"], name: "index_driving_categories_student_details_on_driving_category_id"
    t.index ["student_detail_id"], name: "index_driving_categories_student_details_on_student_detail_id"
  end

  create_table "extra_services", force: :cascade do |t|
    t.string "name"
    t.decimal "price", precision: 8, scale: 2, default: "0.0"
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_extra_services_on_company_id"
  end

  create_table "extra_services_student_details", force: :cascade do |t|
    t.bigint "extra_service_id", null: false
    t.bigint "student_detail_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["extra_service_id"], name: "index_extra_services_student_details_on_extra_service_id"
    t.index ["student_detail_id"], name: "index_extra_services_student_details_on_student_detail_id"
  end

  create_table "installments", force: :cascade do |t|
    t.decimal "price"
    t.date "scheduled_at"
    t.bigint "student_detail_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_detail_id"], name: "index_installments_on_student_detail_id"
  end

  create_table "instructor_details", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "id_doc_series"
    t.string "id_doc_number"
    t.string "id_doc_cnp"
    t.date "id_doc_exp_date"
    t.string "license_series"
    t.date "license_exp_date"
    t.string "certificate_series"
    t.string "certificate_number"
    t.date "certificate_exp_date"
    t.string "medical_number"
    t.date "medical_exp_date"
    t.string "record_number"
    t.date "record_exp_date"
    t.bigint "user_id"
    t.string "psychological_number"
    t.date "psychological_exp_date"
    t.integer "id_doc_type"
    t.index ["user_id"], name: "index_instructor_details_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.string "notifiable_attribute"
    t.boolean "is_resolved"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "order_date"
    t.index ["notifiable_id"], name: "index_notifications_on_notifiable_id"
  end

  create_table "student_details", force: :cascade do |t|
    t.string "old_name"
    t.string "father_first_name"
    t.string "mother_first_name"
    t.date "birthdate"
    t.integer "id_doc_type"
    t.string "id_doc_series"
    t.string "id_doc_number"
    t.string "id_doc_cnp"
    t.integer "sex"
    t.string "phone_number"
    t.string "contact_email"
    t.string "citizenship"
    t.string "address_county"
    t.string "address_city"
    t.string "address_street"
    t.string "address_number"
    t.string "address_building_number"
    t.string "address_entrance_number"
    t.string "address_floor"
    t.string "address_apartment"
    t.string "county_of_birth"
    t.string "city_of_birth"
    t.bigint "contract_number", null: false
    t.date "contract_date"
    t.date "group_date"
    t.integer "duration_in_weeks"
    t.bigint "instructor_id"
    t.string "national_register_number"
    t.string "contract_series"
    t.string "contract_group"
    t.integer "discount_type"
    t.integer "discount_value"
    t.text "discount_reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "id_doc_exp_date"
    t.bigint "user_id"
    t.string "address_country"
    t.string "country_of_birth"
    t.integer "payment_method"
    t.decimal "final_price"
    t.index ["instructor_id"], name: "index_student_details_on_instructor_id"
    t.index ["user_id"], name: "index_student_details_on_user_id"
  end

  create_table "to_dos", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.date "end_date"
    t.integer "priority"
    t.integer "department"
    t.bigint "company_id"
    t.bigint "user_id"
    t.boolean "is_done"
    t.date "done_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "resolution"
    t.index ["company_id"], name: "index_to_dos_on_company_id"
    t.index ["user_id"], name: "index_to_dos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "role"
    t.bigint "company_id", default: 1, null: false
    t.string "first_name"
    t.string "last_name"
    t.index ["company_id"], name: "index_users_on_company_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role"], name: "index_users_on_role"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "make"
    t.string "model"
    t.string "registration"
    t.text "category"
    t.date "itp_date"
    t.date "rca_date"
    t.date "people_insurance_date"
    t.date "first_aid_kit_date"
    t.date "fire_extinguisher_date"
    t.date "fire_extinguisher_2_date"
    t.date "rovigneta_date"
    t.date "itp_tahograf_date"
    t.boolean "vest"
    t.boolean "strap"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "company_id"
    t.bigint "instructor_detail_id"
    t.index ["company_id"], name: "index_vehicles_on_company_id"
    t.index ["instructor_detail_id"], name: "index_vehicles_on_instructor_detail_id"
  end

  add_foreign_key "driving_categories_student_details", "driving_categories"
  add_foreign_key "driving_categories_student_details", "student_details"
  add_foreign_key "extra_services_student_details", "extra_services"
  add_foreign_key "extra_services_student_details", "student_details"
  add_foreign_key "installments", "student_details"
  add_foreign_key "users", "companies"
  add_foreign_key "vehicles", "companies"
  add_foreign_key "vehicles", "instructor_details"
end
