class CreateVehicles < ActiveRecord::Migration[6.0]
  def change
    create_table :vehicles do |t|
      t.string :make
      t.string :type
      t.string :registration
      t.text :category
      t.date :itp_date
      t.date :rca_date
      t.date :people_insurance_date
      t.date :first_aid_kit_date
      t.date :fire_extinguisher_date
      t.date :fire_extinguisher_2_date
      t.date :rovigneta_date
      t.date :itp_tahograf_date
      t.boolean :vest
      t.boolean :strap

      t.timestamps
    end
  end
end
