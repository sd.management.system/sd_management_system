class AddCompanyIdToVehicles < ActiveRecord::Migration[6.0]
  def change
    Vehicle.destroy_all
    add_reference :vehicles, :company, null: false, foreign_key: true
  end
end
