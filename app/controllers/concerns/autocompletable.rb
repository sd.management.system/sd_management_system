module Autocompletable
  extend ActiveSupport::Concern

  private

  def autocomplete_response_for(resources, attribute, search_term)
    resources.where("#{attribute} like ?", "%#{search_term}%")
      .pluck(attribute).map { |v| { label: v, value: v } }
  end
end
