class RenameEmailColumnToStudentEmailInStudentDetails < ActiveRecord::Migration[6.0]
  def change
    rename_column :student_details, :email, :contact_email
  end
end
