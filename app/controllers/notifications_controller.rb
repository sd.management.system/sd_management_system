class NotificationsController < ApplicationController
  include AuthorizableResource
  def update_resolved
    @notification = Notification.find(params[:id])
    authorize @notification, :update?

    if @notification.update(is_resolved: params[:resolve])
      render json: { success: true, message: 'Update successfull'}
    else
      render json: { success: false, message: 'An error occured'}
    end
  end
end
