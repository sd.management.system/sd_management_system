class ToDoPolicy < ApplicationPolicy
  # TODO: resolve policies. investigate todos functionality and update policies
  class Scope < Scope
    def resolve
      scope.none
    end
  end

  def index?
    user.administrator? || user.secretary?
  end

  def new?
    create?
  end

  def create?
    user.administrator? || user.secretary?
  end

  def show?
    return false unless access_company_resource?

    user.administrator? || user.secretary?
  end

  def edit?
    update?
  end

  def update?
    return false unless access_company_resource?

    user.administrator? || user.secretary?
  end

  def destroy?
    return false unless access_company_resource?

    user.administrator?
  end
end
