class RevertSequenceForStudentDetails < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      DROP SEQUENCE contract_number_seq CASCADE;
    SQL
  end

  def down
    execute <<-SQL
      CREATE SEQUENCE contract_number_seq START 1;
      ALTER SEQUENCE contract_number_seq OWNED BY student_details.contract_number;
      ALTER TABLE student_details ALTER COLUMN contract_number SET DEFAULT nextval('contract_number_seq');
    SQL
  end
end
