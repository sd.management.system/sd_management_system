class MakeContractNumberBigInt < ActiveRecord::Migration[6.0]
  def up
    change_column :student_details, :contract_number, :integer, limit: 8, null: false
  end
end
