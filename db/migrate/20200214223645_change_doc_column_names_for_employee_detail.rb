class ChangeDocColumnNamesForEmployeeDetail < ActiveRecord::Migration[6.0]
  def change
    add_column :employee_details, :id_doc_type, :integer

    rename_column :employee_details, :id_series, :id_doc_series
    rename_column :employee_details, :id_number, :id_doc_number
    rename_column :employee_details, :id_uniq_id, :id_doc_cnp
    rename_column :employee_details, :id_exp_date, :id_doc_exp_date
  end
end
