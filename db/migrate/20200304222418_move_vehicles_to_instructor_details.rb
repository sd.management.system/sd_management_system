class MoveVehiclesToInstructorDetails < ActiveRecord::Migration[6.0]
  def up
    Vehicle.destroy_all
    remove_reference :vehicles, :user, index: true, foreign_key: true
    add_reference :vehicles, :instructor_detail, index: true, foreign_key: true
  end
end
