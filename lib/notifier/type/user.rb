module Notifier::Type
  class User < Notifier::Type::Base
    ATTRIBUTES = %i[
      id_doc_exp_date
      license_exp_date
      certificate_exp_date
      medical_exp_date
      psychological_exp_date
      record_exp_date
    ].freeze

    alias user resource
  end
end
