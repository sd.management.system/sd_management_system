class AddResolveResumeToToDos < ActiveRecord::Migration[6.0]
  def change
    add_column :to_dos, :resolution, :text
  end
end
