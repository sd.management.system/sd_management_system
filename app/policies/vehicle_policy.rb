class VehiclePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.administrator? || user.secretary?
        user.company.vehicles
      else
        raise Pundit::NotAuthorizedError, 'Nu este permisa vizualizarea acestei pagini.'
      end
    end
  end

  def index?
    user.administrator? || user.secretary?
  end

  def new?
    create?
  end

  def create?
    user.administrator? || user.secretary?
  end

  def show?
    return false unless access_company_resource?

    user.administrator? || user.secretary?
  end

  def edit?
    update?
  end

  def update?
    return false unless access_company_resource?

    user.administrator?
  end

  def destroy?
    return false unless access_company_resource?

    user.administrator?
  end

  def permitted_attributes
    [
      :make,
      :model,
      :registration,
      :itp_date,
      :rca_date,
      :people_insurance_date,
      :first_aid_kit_date,
      :fire_extinguisher_date,
      :fire_extinguisher_2_date,
      :rovigneta_date,
      :itp_tahograf_date,
      :vest,
      :strap,
      :company_id,
      :instructor_detail_id,
      category: []
    ].freeze
  end
end
