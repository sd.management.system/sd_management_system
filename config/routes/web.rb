resources :companies, except: [:index, :new, :create]

resources :users do
  member do
    get :gdpr
    get :disclaimer
    get :installments, to: "student_details#installments"
    patch :installments, to: "student_details#update"
    put :installments, to: "student_details#update"
  end
  collection do
    get :autocomplete
  end
end

resources :to_dos

resources :vehicles do
  collection do
    get :autocomplete
  end
end

resources :driving_categories do
  collection do
    get :autocomplete
  end
end

resources :extra_services do
  collection do
    get :autocomplete
  end
end

patch 'notifications/update_resolved'