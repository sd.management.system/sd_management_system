class AddPsihologicDataToEmployeeDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :employee_details, :psihologic_number, :string
    add_column :employee_details, :psihologic_exp_date, :date
  end
end
