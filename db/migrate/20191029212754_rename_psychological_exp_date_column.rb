class RenamePsychologicalExpDateColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :employee_details, :psihologic_exp_date, :psychological_exp_date
  end
end
