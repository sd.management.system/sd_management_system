class RemoveFirstNameLastNameFromEmployeeDetails < ActiveRecord::Migration[6.0]
  def change
    remove_column :employee_details, :first_name, :string
    remove_column :employee_details, :last_name, :string
  end
end
