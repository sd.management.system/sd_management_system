class StudentDetailsController < ApplicationController
  include AuthorizableResource
  before_action :authorize_resource, only: %i[
    installments update
  ]

  def installments
    @student_detail = @user.student_detail
  end

  def update
    if @user.student_detail.update(student_detail_params)
      redirect_to user_path(@user)
    else
      @student_detail = @user.student_detail
      render :installments
    end
  end

  private

  def authorize_resource
    @user = User.find(params[:id])
    authorize @user
  end

  def student_detail_params
    params.require(:student_detail)
          .permit(policy(:student_detail).permitted_attributes)
  end
end
