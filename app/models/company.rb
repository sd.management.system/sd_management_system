class Company < ApplicationRecord
  include Notification::NotifiableBase

  attr_accessor :admin_action

  validates :title,
            presence: true
  validates :color,
            :certificate_exp_date,
            :functioning_exp_date,
            :access_key_exp_date,
            :address_city,
            :address_street,
            :address_number,
            presence: true,
            unless: :admin_action?

  has_many :vehicles,
           dependent: :destroy
  has_many :users,
           dependent: :destroy
  has_many :extra_services,
           dependent: :destroy
  has_many :driving_categories,
           dependent: :destroy

  before_validation :set_uniq_title
  # todo: if: -> { title.present? }
  after_update :update_users_usernames

  def full_address
    return @full_address if @full_address.present?

    @full_address = [
      address_city,
      address_street,
      address_number,
      address_building_number,
      address_entrance_number,
      address_floor,
      address_apartment
    ].reject { |c| c.empty? }.join(', ')
  end

  def username_second_part
    "@#{uniq_title}"
  end

  private

  def set_uniq_title
    return false if title.blank?
    return true unless title_changed?

    new_uniq_title = title.gsub(/[^0-9A-Za-z]/, '-').downcase
    i = 1
    while Company.find_by(uniq_title: new_uniq_title)
      new_uniq_title = title.gsub(/[^0-9A-Za-z]/, '-').downcase + "-#{i}"
      i += 1
    end
    self.uniq_title = new_uniq_title
  end

  def update_users_usernames
    return true unless saved_changes["title"].present?

    users.find_each do |user|
      user.update(username: user.username_first_part + username_second_part)
    end
  end

  def admin_action?
    @admin_action
  end
end
