# Notification is used to retrive alerts for users
# - expiration dates form user data
# - expiration date for user assets (vehicles, to_dos etc.)
class Notifier::Client
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def all
    @all ||= company_notifications.or(to_dos_notifications).or(vehicles_notifications).or(user_notifications)
  end

  def four_months_range
    @four_months_range ||= all.where('order_date > ? AND order_date < ?', 2.months.ago, 2.months.from_now)
  end

  def ordered
    @ordered ||= all.order(:order_date)
  end

  def four_months_range_ordered
    @four_months_range_ordered ||= four_months_range.order(:order_date)
  end

  def self.message(notification)
    "Notifier::Type::#{notification.notifiable_type}".constantize.new(notification.notifiable).message_for(notification)
  end

  def company_notifications
    Notifier::Type::Company.new(user.company).all
  end

  def to_dos_notifications
    Notifier::Type::ToDo.batch_notifications(user.to_dos)
  end

  def vehicles_notifications
    Notifier::Type::Vehicle.batch_notifications(user.vehicles)
  end

  def user_notifications
    Notifier::Type::User.new(user).all
  end
end
