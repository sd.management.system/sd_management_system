class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.administrator? || user.secretary?
        User.where(company_id: user.company_id)
      else
        raise Pundit::NotAuthorizedError, 'Nu este permisa vizualizarea utilizatorilor decat de administratori sau secretari.'
      end
    end
  end

  def index?
    user.administrator? || user.secretary?
  end

  def new?
    create?
  end

  # admins cannot be edited, just created
  def create?
    return true if user.administrator?

    (user.administrator? || user.secretary?) &&
      (record.secretary? || record.instructor?)
  end

  def show?
    return false unless access_company_resource?

    user.administrator? || user.secretary?
  end

  def edit?
    update?
  end

  def update?
    return false unless access_company_resource?

    user.administrator?
  end

  def installments?
    update?
  end

  def destroy?
    return false unless access_company_resource?

    user.administrator?
  end

  def gdpr?
    show? && record.student?
  end

  def disclaimer?
    show? && record.student?
  end

  def can_have_instructor_detail?
    record.instructor?
  end

  def can_have_student_detail?
    record.student?
  end

  def accessible_roles
    if record.administrator?
      User.roles
    else
      User.roles.except(:administrator)
    end
  end

  def permitted_attributes
    instructor_detail_attributes = %i[
      id_doc_type
      id_doc_series
      id_doc_number
      id_doc_cnp
      id_doc_exp_date
      license_series
      license_exp_date
      certificate_series
      certificate_number
      certificate_exp_date
      medical_number
      medical_exp_date
      record_number
      record_exp_date
      psychological_number
      psychological_exp_date
    ].freeze

    student_detail_attributes = [
      :id,
      :old_name,
      :father_first_name,
      :mother_first_name,
      :birthdate,
      :id_doc_type,
      :id_doc_series,
      :id_doc_number,
      :id_doc_cnp,
      :sex,
      :phone_number,
      :contact_email,
      :citizenship,
      :address_country,
      :address_county,
      :address_city,
      :address_street,
      :address_number,
      :address_building_number,
      :address_entrance_number,
      :address_floor,
      :address_apartment,
      :country_of_birth,
      :county_of_birth,
      :city_of_birth,
      :contract_date,
      :group_date,
      :duration_in_weeks,
      :instructor_id,
      :national_register_number,
      :contract_series,
      :contract_group,
      :payment_method,
      :discount_type,
      :discount_value,
      :discount_reason,
      :id_doc_exp_date,
      :final_price,
      driving_category_ids: [],
      extra_service_ids: []
    ].freeze

    [
      :username,
      :first_name,
      :last_name,
      :password,
      :password_confirmation,
      :role,
      instructor_detail_attributes: instructor_detail_attributes,
      student_detail_attributes: student_detail_attributes
    ].freeze
  end
end
