module AuthorizableResource
  extend ActiveSupport::Concern

  included do
    include Pundit
    after_action :verify_authorized, except: %i[index autocomplete]
    after_action :verify_policy_scoped, only: %i[index autocomplete]
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  end

  private

  def user_not_authorized(exception)
    flash[:danger] = exception.message
    redirect_to(request.referrer || root_path)
  end
end
