class Installment < ApplicationRecord
  validates :price,
            :scheduled_at,
            presence: true

  belongs_to :student_detail
end
