class NotificationPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      Notification.none
    end
  end

  def index?
    false
  end

  def new?
    create?
  end

  def create?
    false
  end

  def show?
    false
  end

  def edit?
    false
  end

  def update?
    Notifier::Client.new(user).all.find_by(id: record).present?
  end

  def destroy?
    false
  end
end

