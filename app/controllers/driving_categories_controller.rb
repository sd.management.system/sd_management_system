class DrivingCategoriesController < ApplicationController
  include AuthorizableResource
  before_action :authorize_resources, only: %i[
    index autocomplete
  ]
  before_action :authorize_resource, only: %i[
    show edit update destroy
  ]

  def index; end

  def new
    @driving_category = DrivingCategory.new
    authorize(@driving_category)
  end

  def create
    @driving_category = current_company
                        .driving_categories
                        .new(driving_category_params)
    authorize(@driving_category)
    if @driving_category.save
      redirect_to driving_category_path(@driving_category), notice: 'Categorie adaugat cu success'
    else
      render :new
    end
  end

  def show; end

  def edit
    @driving_category = DrivingCategory.find(params[:id])
    authorize(@driving_category)
  end

  def update
    if @driving_category.update(driving_category_params)
      flash[:success] = "Categories schimbat cu success"
      redirect_to driving_category_path(@driving_category)
    else
      render :edit
    end
  end

  def destroy
    @driving_category.destroy
    flash[:success] = "Categories stears cu success"
    redirect_to driving_categories_path
  end

  def autocomplete
    render json: autocomplete_response_for(@driving_categories)
  end

  private

  def authorize_resources
    @driving_categories = policy_scope(DrivingCategory)
  end

  def authorize_resource
    @driving_category = DrivingCategory.find(params[:id])
    authorize @driving_category
  end

  def driving_category_params
    params.require(:driving_category)
          .permit(policy(:driving_category).permitted_attributes)
  end
end
