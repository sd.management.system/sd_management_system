require 'active_support/concern'

module Notification::NotifiableBase
  extend ActiveSupport::Concern

  included do
    after_save :create_notifiactions
    has_many :notifications,
             as: :notifiable,
             dependent: :destroy
  end

  private

  def create_notifiactions
    notifier_class.new(self).create_notifications
  end

  def notifiable_class_name
    self.class.name.split('::').last
  end

  def notifiable_class
    notifiable_class_name.constantize
  end

  def notifier_class
    "Notifier::Type::#{notifiable_class_name}".constantize
  end
end
