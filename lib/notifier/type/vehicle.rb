module Notifier::Type
  class Vehicle < Notifier::Type::Base
    ATTRIBUTES = %i[itp_date rca_date people_insurance_date first_aid_kit_date fire_extinguisher_date fire_extinguisher_2_date rovigneta_date itp_tahograf_date].freeze
    alias vehicle resource
  end
end
