class Comment < ApplicationRecord
  ALLOWED_TYPES = %w[Notification ToDo].freeze

  belongs_to :commentable,
             polymorphic: true

  validates :comentable_type,
            presence: true,
            inclusion: { in: ALLOWED_TYPES }
end
