class RenameNotificationsColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :notifications, :notiifable_type, :notifiable_type
  end
end
