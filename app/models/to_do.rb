class ToDo < ApplicationRecord
  include Notification::NotifiableBase

  enum priority: { scazuta: 1, medie: 2, ridicata: 3 }

  validate :end_date_xor_priority_presence
  validate :company_xor_user_presence
  validate :department_xor_user_presence

  belongs_to :company,
             required: false
  belongs_to :user,
             required: false
  has_many :comments,
           as: :commentable

  private

  def end_date_xor_priority_presence
    return if end_date.present? ^ priority.present?

    errors.add(:end_date, 'sau prioritate trebuie selectate unul si doar unul')
    errors.add(:priority, 'sau data limita trebuie selectate unul si doar unul')
  end

  def company_xor_user_presence
    return if company.present? ^ user.present?

    errors.add(:company_id, 'sau utilizator trebuie selectate unul si doar unul')
    errors.add(:user_id, 'sau companie trebuie selectate unul si doar unul')
  end

  def department_xor_user_presence
    return if department.present? ^ user.present?

    errors.add(:department, 'sau utilizator trebuie selectate unul si doar unul')
    errors.add(:user_id, 'sau departament trebuie selectate unul si doar unul')
  end
end
