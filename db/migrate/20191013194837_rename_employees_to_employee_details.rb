class RenameEmployeesToEmployeeDetails < ActiveRecord::Migration[6.0]
  def change
    rename_table :employees, :employee_details
  end
end
