class Notification < ApplicationRecord
  validates :notifiable_id,
            uniqueness: {
              scope: %i[notifiable_type notifiable_attribute],
              message: 'should exist once for a resource for one attribute'
            }
  validates :order_date,
            presence: true

  scope :resolved, -> { where(is_resolved: true) }
  scope :unresolved, -> { where(is_resolved: [nil, false]) }

  belongs_to :notifiable,
             polymorphic: true
  has_many :comments,
           as: :commentable
end
