class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :notifiable_id
      t.string :notiifable_type
      t.string :notifiable_attribute
      t.boolean :is_resolved

      t.timestamps
    end
    add_index :notifications, :notifiable_id
  end
end
