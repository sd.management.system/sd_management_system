class CompanyPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      Company.where(id: user.company_id)
    end
  end

  def index?
    false
  end

  def new?
    false
  end

  def create?
    false
  end

  def show?
    return false if user.company_id != record.id

    user.administrator? || user.secretary?
  end

  def edit?
    show?
  end

  def update?
    return false if user.company_id != record.id

    user.administrator? || user.secretary?
  end

  def destroy?
    return false if user.company_id != record.id

    user.administrator?
  end

  def permitted_attributes
    %i[
      title
      color
      certificate_exp_date
      functioning_exp_date
      access_key_exp_date
      address_county
      address_city
      address_street
      address_number
      address_building_number
      address_entrance_number
      address_floor
      address_apartment
    ].freeze
  end
end
