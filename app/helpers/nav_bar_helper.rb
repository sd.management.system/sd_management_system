module NavBarHelper
  NAV_LINK_ITEMS = %i[users to_dos vehicles driving_categories extra_services].freeze

  def nav_link_titles
    return @nav_link_titles if @nav_link_titles.present?

    @nav_link_titles ||= {
      dashboard: 'Dashboard',
      company: t('activerecord.models.company.one'),
      users: t('activerecord.models.user.other'),
      to_dos: t('activerecord.models.to_do.other'),
      vehicles: t('activerecord.models.vehicle.other'),
      driving_categories: t('activerecord.models.driving_category.other'),
      extra_services: t('activerecord.models.extra_service.other')
    }
  end

  def nav_item_links
    nav_items = nav_item(:dashboard) if DashboardPolicy.new(current_user).show?
    nav_items = nav_item(:company) if DashboardPolicy.new(current_user).show?
    NAV_LINK_ITEMS.each do |nav_link_item|
      nav_items += nav_item(nav_link_item) if policy(nav_link_item.to_s.classify.constantize).index?
    end
    nav_items.html_safe
  end

  def nav_item(nav_for)
    li_class = 'nav-item'
    li_class += ' active' if controller_name.to_sym == nav_for
    content_tag :li, class: li_class do
      link_to(nav_link_titles[nav_for], nav_link_path_helper[nav_for], class: 'nav-link')
    end
  end

  private

  def nav_link_path_helper
    {
      dashboard: root_path,
      company: company_path(current_company),
      users: users_path,
      to_dos: to_dos_path,
      vehicles: vehicles_path,
      driving_categories: driving_categories_path,
      extra_services: extra_services_path,
    }.freeze
  end
end
