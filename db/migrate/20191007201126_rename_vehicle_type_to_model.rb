class RenameVehicleTypeToModel < ActiveRecord::Migration[6.0]
  def change
    rename_column :vehicles, :type, :model
  end
end
