class UsersController < ApplicationController
  include AuthorizableResource
  include Autocompletable
  before_action :authorize_resources, only: %i[
    index autocomplete
  ]
  before_action :authorize_resource, only: %i[
    show edit update destroy gdpr disclaimer
  ]

  def index; end

  def new
    @user = current_company.users.new
    authorize(@user)
  end

  def create
    @user = current_company.users.new(user_params)
    authorize(@user)
    if @user.save
      flash[:success] = 'Utilizator adaugat cu success'
      redirect_to user_path(@user)
    else
      render :new
    end
  end

  def show
    @instructor_detail = @user.instructor_detail
    @student_detail = @user.student_detail
  end

  def edit; end

  def update
    update_success = if user_params[:password].present?
                       @user.update(user_params)
                     else
                       @user.update_without_password(user_params)
                     end
    if update_success
      flash[:success] = 'Utilizator schimbat cu success'
      redirect_to user_path(@user)
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    flash[:success] = 'Utilizator sters cu success'
    redirect_to users_path
  end

  def autocomplete
    resources, attribute = case params[:for].split('.')[0]
                when 'student_details'
                  [StudentDetail.all, params[:for].split('.')[1]]
                when 'instructor_details'
                  [InstructorDetail.all, params[:for].split('.')[1]]
                else
                  [@users, params[:for]]
                end

    render json: autocomplete_response_for(resources, attribute, params[:q])
  end

  def gdpr
    @student_detail = @user.student_detail
    render pdf: 'gdpr', formats: :pdf
  end

  def disclaimer
    render pdf: 'disclaimer', formats: :pdf
  end

  private

  def authorize_resources
    @users = policy_scope(User)
  end

  def authorize_resource
    @user = User.find(params[:id])
    authorize @user
  end

  def user_params
    @user_params = params.require(:user)
                         .permit(policy(:user).permitted_attributes)

    if @user_params[:role] == "student"
      @user_params.except(:instructor_detail_attributes)
    elsif @user_params[:role] == "instructor"
      @user_params.except(:student_detail_attributes)
    end

    @user_params
  end
end
