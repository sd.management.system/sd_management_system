class RenameCnpToIdDocCnpInStudentDetail < ActiveRecord::Migration[6.0]
  def change
    rename_column :student_details, :cnp, :id_doc_cnp
  end
end
