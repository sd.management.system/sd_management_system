class AddPaymentMethodToStudentDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :student_details, :payment_method, :integer
  end
end
