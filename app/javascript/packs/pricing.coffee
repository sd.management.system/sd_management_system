document.addEventListener 'turbolinks:load', () ->
  $('#user_student_detail_attributes_discount_type').on 'keyup', () ->
    update_estimated_price()
  $('#user_student_detail_attributes_discount_value').on 'keyup', () ->
    update_estimated_price()
  $('#user_student_detail_attributes_driving_category_ids').on 'change', () ->
    update_estimated_price()
  $('#user_student_detail_attributes_extra_service_ids').on 'change', () ->
    update_estimated_price()

  $('#user_student_detail_attributes_discount_type').on 'change', () ->
    reset_discount_value()

  $('#user_student_detail_attributes_discount_value').on 'change', () ->
    validate_discount_value()

update_estimated_price = () ->
  if $('#user_student_detail_attributes_discount_type').val() == ''
    $('#user_student_detail_attributes_final_price').tooltip('disable');
  else
    calculated_price = 0;
    $('#user_student_detail_attributes_driving_category_ids').val().forEach (id) ->
      calculated_price +=  parseFloat(DRIVING_CATEOGRY_PRICES[id])
    $('#user_student_detail_attributes_extra_service_ids').val().forEach (id) ->
      calculated_price +=  parseFloat(EXTRA_SERVICE_PRICES[id])

    if $('#user_student_detail_attributes_discount_type').val() == 'fixed'
      calculated_price -= $('#user_student_detail_attributes_discount_value').val()
    else if $('#user_student_detail_attributes_discount_type').val() == 'percentage'
      calculated_price -= (calculated_price * $('#user_student_detail_attributes_discount_value').val()) / 100


    $('#user_student_detail_attributes_final_price').attr('title', 'estimated price: ' + calculated_price)
    $('#user_student_detail_attributes_final_price').tooltip(
      position: { my: 'right-20', at: 'left', of: 'label[for=user_student_detail_attributes_final_price]' }
    )
    $('#user_student_detail_attributes_final_price').tooltip('enable');
  0

reset_discount_value = () ->
  $('#user_student_detail_attributes_discount_value').val(0)
  update_estimated_price()

validate_discount_value = () ->
  value = $('#user_student_detail_attributes_discount_value').val()
  if $('#user_student_detail_attributes_discount_type').val() == 'percentage' &&  (value > 100 || value < 1)
    alert('Value must be between 0 and 100, including 0 and 100')
    reset_discount_value()
