class CreateStudentDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :student_details do |t|
      t.string :old_name
      t.string :father_first_name
      t.string :mother_first_name
      t.date :birthdate
      t.integer :id_doc_type
      t.string :id_doc_series
      t.string :id_doc_number
      t.string :cnp
      t.integer :sex
      t.string :telephone
      t.string :email
      t.string :citizenship
      t.string :county
      t.string :city
      t.string :street
      t.string :number
      t.string :building_number
      t.string :entrance_number
      t.string :floor
      t.string :apartment
      t.string :place_of_birth
      t.string :county_of_birth
      t.string :city_of_birth
      t.integer :contract_number
      t.date :contract_date
      t.date :group_date
      t.integer :duration_in_weeks
      t.references :instructor
      t.string :national_register_number
      t.string :contract_series
      t.string :contract_group
      t.integer :discount_type
      t.integer :discount_value
      t.text :discount_reason

      t.timestamps
    end
  end
end
