class Notification::User < Notification
  default_scope { where(notifiable_type: 'User') }
end
