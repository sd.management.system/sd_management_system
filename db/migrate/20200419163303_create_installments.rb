class CreateInstallments < ActiveRecord::Migration[6.0]
  def change
    create_table :installments do |t|
      t.decimal :price
      t.date :scheduled_at
      t.references :student_detail, null: false, foreign_key: true

      t.timestamps
    end
  end
end
