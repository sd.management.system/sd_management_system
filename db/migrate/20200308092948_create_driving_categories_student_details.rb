class CreateDrivingCategoriesStudentDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :driving_categories_student_details do |t|
      t.references :driving_category, null: false, foreign_key: true
      t.references :student_detail, null: false, foreign_key: true

      t.timestamps
    end
  end
end
