class RemoveNotNullForVehiclesUsers < ActiveRecord::Migration[6.0]
  def change
    change_column :vehicles, :user_id, :integer, null: true
  end
end
