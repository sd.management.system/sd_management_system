class DrivingCategoriesStudentDetail < ApplicationRecord
  belongs_to :driving_category
  belongs_to :student_detail
end
