document.addEventListener 'turbolinks:load', () ->
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()

  initialize_autocomplete()


initialize_autocomplete = ->
  $('.autocomplete').autocomplete(
    delay: 100
    minLength: 3
    autocomplete: 'none'
    source: (request, response) ->
      type = $(this.element).data().autocompleteType
      make = $(this.element).data().autocompleteFor
      url = '/' + type + '/autocomplete'
      $.getJSON(url, { for: make, q: $(this.element).val() }, response)
  )
  $('.users_form .autocomplete').attr('autocomplete', 'none');
  $('.vehicles_form .autocomplete').attr('autocomplete', 'off');
  0
