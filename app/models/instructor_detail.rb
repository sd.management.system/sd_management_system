class InstructorDetail < ApplicationRecord
  enum id_doc_type: { ci: 0, bi: 1 }

  validates :id_doc_series,
            :id_doc_number,
            :id_doc_cnp,
            :id_doc_type,
            :id_doc_exp_date,
            :license_series,
            :license_exp_date,
            :certificate_series,
            :certificate_number,
            :certificate_exp_date,
            :medical_number,
            :medical_exp_date,
            :record_number,
            :record_exp_date,
            :user,
            :psychological_number,
            :psychological_exp_date,
            presence: true
  validates :id_doc_cnp,
            uniqueness: true

  belongs_to :user
  has_many :vehicles,
           dependent: :nullify
end
