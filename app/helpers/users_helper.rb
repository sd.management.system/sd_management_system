module UsersHelper
  def form_employee_detail_for_user(user)
    user.instructor_detail || user.build_instructor_detail
  end

  def hide_instructor_detail_form(user, html_class = '')
    html_class += ' d-none' unless policy(user).can_have_instructor_detail?
    html_class
  end

  def form_student_detail_for_user(user)
    user.student_detail || user.build_student_detail
  end

  def hide_student_detail_form(user, html_class = '')
    html_class += ' d-none' unless policy(user).can_have_student_detail?
    html_class
  end
end
