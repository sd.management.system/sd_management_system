class StudentDetail < ApplicationRecord
  enum id_doc_type: { ci: 0, bi: 1 }
  enum sex: { M: 0, F: 1 }
  enum payment_method: { integral_payment: 0, installments_payment: 1}
  enum discount_type: { fixed: 0, percentage: 1 }

  validates :birthdate,
            :id_doc_type,
            :id_doc_series,
            :id_doc_number,
            :id_doc_cnp,
            :id_doc_exp_date,
            :sex,
            :phone_number,
            :citizenship,
            :address_country,
            :address_county,
            :address_city,
            :address_number,
            :country_of_birth,
            :county_of_birth,
            :city_of_birth,
            :contract_number,
            :contract_date,
            :contract_group,
            :duration_in_weeks,
            :payment_method,
            :final_price,
            presence: true
  validates :id_doc_number,
            length: { is: 6 }
  validates :id_doc_cnp,
            format: { with: /\A\d{13}\z/i }
  validates :contract_number,
            uniqueness: true
  validates :discount_value,
            :discount_reason,
            presence: true,
            if: -> { discount_type.present? }
  validate :discount_corectness,
           if: -> { discount_type.present? }

  validates_associated :installments
  validate :installments_total

  belongs_to :user
  belongs_to :instructor,
             foreign_key: :instructor_id,
             class_name: 'User'
  has_many :driving_categories_student_details,
           dependent: :destroy
  has_many :driving_categories,
           through: :driving_categories_student_details
  has_many :extra_services_student_details,
           dependent: :destroy
  has_many :extra_services,
           through: :extra_services_student_details
  has_many :installments,
           dependent: :destroy

  before_validation :set_contract_number
  after_save :create_default_installment

  accepts_nested_attributes_for :installments,
                                allow_destroy: true,
                                reject_if: :all_blank

  def next_contract_number
    sd = StudentDetail
         .select('MAX(student_details.contract_number) as max_contract_number')
         .joins('INNER JOIN users ON users.id = student_details.user_id')
         .where('users.company_id = ?', user.company_id)
         .group('student_details.id')
         .order('max_contract_number DESC')
         .first
    (sd&.max_contract_number || 0) + 1
  end

  private

  def set_contract_number
    self.contract_number ||= next_contract_number
  end

  def discount_corectness
    return true if discount_value.blank?
    return true unless discount_type == 'percentage' && discount_value > 100

    errors.add(:discount_value, :percentage_discount_type_value)
  end

  def create_default_installment
    self.installments.create(price: final_price, scheduled_at: Date.today) if installments_payment? && installments.blank?
  end

  def installments_total
    sum = 0
    self.installments.each do |installment|
      sum += installment.price if installment.price.present? && !installment.marked_for_destruction?
    end
    errors.add(:installments, :incorrect_sum) if sum != final_price
  end
end
