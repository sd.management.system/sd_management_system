class InstructorDetailPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.administrator? || user.secretary?
        InstructorDetail.joins('INNER JOIN users ON users.id = instructor_details.user_id')
                        .where('users.company_id = ?', user.company_id)
      else
        raise Pundit::NotAuthorizedError, 'Nu este permisa vizualizarea utilizatorilor decat de administratori sau secretari.'
      end
    end
  end
end
