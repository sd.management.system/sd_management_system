class CompaniesController < ApplicationController
  include AuthorizableResource
  before_action :authorize_resource, only: %i[
    show edit update
  ]

  def show; end

  def edit
    @company = Company.find(params[:id])
    authorize(@company)
  end

  def update
    if @company.update(company_params)
      flash[:success] = 'Utilizator schimbat cu success'
      redirect_to company_path(@company)
    else
      render :edit
    end
  end

  private

  def authorize_resource
    @company = Company.find(params[:id])
    authorize @company
  end

  def company_params
    params.require(:company)
          .permit(policy(:company).permitted_attributes)
  end
end
