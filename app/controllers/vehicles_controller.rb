class VehiclesController < ApplicationController
  include AuthorizableResource
  include Autocompletable
  before_action :authorize_resources, only: %i[
    index autocomplete
  ]
  before_action :authorize_resource, only: %i[
    show edit update destroy
  ]

  def index; end

  def new
    @vehicle = current_company.vehicles.new
    authorize(@vehicle)
  end

  def create
    @vehicle = current_company.vehicles.new(vehicle_params)
    authorize(@vehicle)
    if @vehicle.save
      redirect_to vehicle_path(@vehicle), notice: 'Angajat adaugat cu success'
    else
      render :new
    end
  end

  def show; end

  def edit
    @vehicle = Vehicle.find(params[:id])
    authorize(@vehicle)
  end

  def update
    if @vehicle.update(vehicle_params)
      flash[:success] = "Utilizator schimbat cu success"
      redirect_to vehicle_path(@vehicle)
    else
      render :edit
    end
  end

  def destroy
    @vehicle.destroy
    flash[:success] = "Utilizator sters cu success"
    redirect_to vehicles_path
  end

  def autocomplete
    render json: autocomplete_response_for(@vehicles, params[:for], params[:q])
  end

  private

  def authorize_resources
    @vehicles = policy_scope(Vehicle)
  end

  def authorize_resource
    @vehicle = Vehicle.find(params[:id])
    authorize @vehicle
  end

  def vehicle_params
    params.require(:vehicle)
          .permit(policy(:vehicle).permitted_attributes)
  end
end
