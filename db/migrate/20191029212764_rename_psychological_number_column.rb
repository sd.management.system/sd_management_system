class RenamePsychologicalNumberColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :employee_details, :psihologic_number, :psychological_number
  end
end
