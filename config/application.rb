require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SdManagementSystem
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # internationalization, language, translate
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    I18n.available_locales = [:ro, :en]
    config.i18n.default_locale = :ro

    # load lib folder with modules and classes
    config.autoload_paths << Rails.root.join('lib')
    # load models subdirectories
    config.autoload_paths << Rails.root.join('/app/models/notification/*')
  end
end
