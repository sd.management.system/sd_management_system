class User < ApplicationRecord
  include Notification::NotifiableBase
  include Detailable::User

  devise :database_authenticatable, :rememberable, :validatable,
         authentication_keys: [:username]

  enum role: { administrator: 0, secretary: 1, instructor: 2, student: 3 }

  validates :username,
            :first_name,
            :last_name,
            :role,
            :company,
            presence: true
  validates :username,
            uniqueness: true
  validates :instructor_detail,
            presence: true, if: :instructor?
  validates :student_detail,
            presence: true, if: :student?
  validates_associated :instructor_detail,
                       :student_detail

  belongs_to :company
  has_one :instructor_detail, # is used for instructor users
          dependent: :destroy
  has_one :student_detail, # is used for student users
          dependent: :destroy
  has_many :vehicles,
           through: :instructor_detail
  has_many :installments,
           through: :student_detail

  accepts_nested_attributes_for :instructor_detail,
                                :student_detail

  before_validation :build_company_username
  before_validation :set_instructor_detail_existence,
                    :set_student_detail_existence

  def full_name
    "#{first_name} #{last_name}"
  end

  def to_dos
    # todos pertaining a user or user's company and are from user's department OR no department
    ToDo.where(company_id: nil, user_id: id)
        .or(ToDo.where(user_id: nil, company_id: company_id))
        .or(ToDo.where(department: [nil, User.roles[role]], company_id: company_id))
  end

  def username_first_part
    username.split('@')[0]
  end
  delegate :username_second_part, to: :company

  private

  def set_instructor_detail_existence
    self.instructor_detail = nil unless instructor?
  end

  def set_student_detail_existence
    self.student_detail = nil unless student?
  end

  def build_company_username
    return false if username.blank?

    self.username = username_first_part + username_second_part
  end

  # devise bypass email validations, we are using username for authenticating
  def email_required?
    false
  end
  def will_save_change_to_email?
    false
  end
end
