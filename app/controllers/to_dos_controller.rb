class ToDosController < ApplicationController
  include AuthorizableResource
  before_action :authorize_resources, only: %i[
    index
  ]
  before_action :authorize_resource, only: %i[
    show edit update destroy
  ]

  def index; end

  def new
    @to_do = ToDo.new
    authorize(@to_do)
  end

  def create
    @to_do = ToDo.new(to_do_params)
    authorize(@to_do)
    if @to_do.save
      redirect_to to_do_path(@to_do), notice: 'ToDo adaugat cu success'
    else
      render :new
    end
  end

  def show; end

  def edit
    @to_do = ToDo.find(params[:id])
    authorize(@to_do)
  end

  def update
    if @to_do.update(to_do_params)
      flash[:success] = 'Utilizator schimbat cu success'
      redirect_to to_do_path(@to_do)
    else
      render :edit
    end
  end

  def destroy
    @to_do.destroy
    flash[:success] = 'To Do sters cu success'
    redirect_to to_dos_path
  end

  private

  def authorize_resources
    @to_dos = policy_scope(ToDo)
  end

  def authorize_resource
    @to_do = ToDo.find(params[:id])
    authorize @to_do
  end

  def to_do_params
    params.require(:to_do)
          .permit(
            :title,
            :description,
            :end_date,
            :priority,
            :department,
            :company_id,
            :user_id,
            :is_done,
            :done_date
          )
  end
end
