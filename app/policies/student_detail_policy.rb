class StudentDetailPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.administrator? || user.secretary?
        StudentDetail.joins('INNER JOIN users ON users.id = student_details.user_id')
                     .where('users.company_id = ?', user.company_id)
      else
        raise Pundit::NotAuthorizedError, 'Nu este permisa vizualizarea utilizatorilor decat de administratori sau secretari.'
      end
    end
  end

  def permitted_attributes
    installments_attributes = [
      :id,
      :_destroy,
      :scheduled_at,
      :price
    ].freeze

    [
      installments_attributes: installments_attributes
    ].freeze
  end
end
