class ExtraServicePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.administrator? || user.secretary?
        user.company.extra_services
      else
        raise Pundit::NotAuthorizedError, 'Nu este permisa vizualizarea acestei pagini.'
      end
    end
  end

  def index?
    user.administrator? || user.secretary?
  end

  def new?
    create?
  end

  def create?
    user.administrator? || user.secretary?
  end

  def show?
    return false unless access_company_resource?

    user.administrator? || user.secretary?
  end

  def edit?
    update?
  end

  def update?
    return false unless access_company_resource?

    user.administrator? || user.secretary?
  end

  def destroy?
    return false unless access_company_resource?

    user.administrator?
  end

  def permitted_attributes
    %i[
      name
      price
    ].freeze
  end
end
