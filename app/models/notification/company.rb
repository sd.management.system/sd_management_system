class Notification::Company < Notification
  default_scope { where(notifiable_type: 'Company') }
end
