class AddOrderDateToNotifications < ActiveRecord::Migration[6.0]
  def change
    add_column :notifications, :order_date, :date
  end
end
