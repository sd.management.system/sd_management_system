class AddFinalPriceToStudentDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :student_details, :final_price, :decimal
  end
end
