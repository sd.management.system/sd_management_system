class AddAddressToCompanies < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :address_county, :string
    add_column :companies, :address_city, :string
    add_column :companies, :address_street, :string
    add_column :companies, :address_number, :string
    add_column :companies, :address_building_number, :string
    add_column :companies, :address_entrance_number, :string
    add_column :companies, :address_floor, :string
    add_column :companies, :address_apartment, :string
  end
end
