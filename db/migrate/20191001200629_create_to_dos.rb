class CreateToDos < ActiveRecord::Migration[6.0]
  def change
    create_table :to_dos do |t|
      t.string :title
      t.text :description
      t.date :end_date
      t.integer :priority
      t.integer :department
      t.references :company
      t.references :user
      t.boolean :is_done
      t.date :done_date

      t.timestamps
    end
  end
end
