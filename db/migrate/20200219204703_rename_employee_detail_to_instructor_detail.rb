class RenameEmployeeDetailToInstructorDetail < ActiveRecord::Migration[6.0]
  def change
    rename_table :employee_details, :instructor_details
  end
end
