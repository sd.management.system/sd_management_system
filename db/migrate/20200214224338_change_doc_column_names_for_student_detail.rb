class ChangeDocColumnNamesForStudentDetail < ActiveRecord::Migration[6.0]
  def change
    add_column :student_details, :id_doc_exp_date, :date
  end
end
